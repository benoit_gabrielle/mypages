# Model documentation
Last updated 26 January 2024 by Benoît Gabrielle

This is a short documentation to the CERES-EGC model, a version of the CERES model developed at INRA to simulate the environmental impacts of crops. CERES-EGC may be obtained from the download page.

The latest developments are explained in the log file.

## Brief description
CERES-EGC was adapted from the CERES suite of soil-crop models (Jones and Kiniry, 1986), with a focus on the simulation of environmental outputs such as nitrate leaching and gaseous emissions of N2O, ammonia and nitrogen oxides (Gabrielle et al., 2006). CERES-EGC comprises sub-models for the major processes governing the cycles of water, carbon and nitrogen in soil-crop systems. 

A physical module simulates the transfer of heat, water and nitrate down the soil profile, as well as soil evaporation, plant water uptake and transpiration in relation to climatic demand. A microbiological module simulates the turnover of organic matter in the plow layer, involving both mineralization and immobilization of inorganic N. Crop net photosynthesis is a linear function of intercepted radiation
 according to the Monteith approach, with interception depending on leaf area index based on Beer's law of diffusion in turbid media.
Photosynthates are partitioned on a daily basis to currently growing organs (roots, leaves, stems, fruit) according to crop development stage. The latter is driven by the accumulation of growing degree days, as well as cold temperature and day-length for crops sensitive to vernalization and photoperiod. Crop N uptake is computed through a supply/demand scheme, with soil supply depending on soil nitrate and ammonium concentrations and root length density. 

CERES-EGC simulates a large number of crop species, including: winter wheat, sugar beet, maize, sunflower, winter oilseed rape, winter and spring barley, pea, soybean, faba bean, miscanthus and switchgrass. CERES-EGC runs on a daily time step, using the following weather data as input: rainfall, Penman potential evapotranspiration, min and max air temperatures, solar radiation, and wind speed. It can simulate any sequence of crops over time, including bare fallow periods between two successive crops, or catch crops.

## Major changes with respect to CERES
Readers may refer to the original documentation of CERES (eg, Jones and Kiniry, 1986), for we thereafter focus on the changes relative to this model.

The water balance submodel of CERES-EGC is taken from Gabrielle et al., (1995), who have implemented a semi-empirical Darcy's law for water movement in the soil profile in both saturated and unsaturated conditions. It also can simulate contribution upward water fluxes into the root zone from the groundwater table. It requires three new parameters which are detailed in  the input/output files section. A new subroutine for soil temperature from Hoffmann et al. (1993) has been included. The submodel for soil C-N transformations is an adapted version of NCSOIL (Molina et al., 1983), described in Gabrielle and Kengni (1996). Its parameters may be calculated from the original CERES parameters, which include soil organic carbon and nitrogen, and the amount of carbon and nitrogen in the unharvested residues of the preceding crop (see the parameterisation section). 

CERES-EGC includes NOE (Hénault et al., 2005), a semi-empirical sub-model simulating the production and reduction of N2O in
agricultural soils through both the denitrification and nitrification pathways.The denitrification component of NOE is based on NEMIS (Hénault et al., 2000), a model that expresses total denitrification of soil nitrate as the product of a potential rate with three unitless factors related to soil water content, nitrate content, and temperature. The fraction of denitrified nitrate that evolves as N2O is considered as constant for a given soil type, according to the experimental evidence provided by (Hénault et al., 2001).In a similar fashion, nitrification is modeled as a Michaëlis-Menten reaction, with NH4+ as substrate. The corresponding rate is multiplied by unitless modifiers related to soil water content and temperature. As for denitrification, a soil-specific proportion of total nitrification evolves as N2O, following the results of Garrido et al. (2002).
The two pathways are connected in that nitrification-derived N2O may be reduced to N2 by denitrification, should the two processes be simultaneously active. 

