
# Publications on CERES-EGC
Last updated 20 January 2012 by Benoît Gabrielle
Modelling concepts | Model testing | Model application | Miscellaneous

## Modelling concepts
B. Gabrielle, S. Menasseri, and S. Houot. Analysis and field-evaluation of the CERES models' water balance component. Soil Sci. Soc. Am. J. 59:1402-1411, 1995.

B. Gabrielle and L. Kengni. Analysis and eld-evaluation of the CERES models' soil components : Nitrogen transfer and transformation. Soil Sci. Soc. Am. J. 60 :142-149, 1996.

B. Gabrielle, P. Denoroy, G. Gosse, E. Justes, and M. N. Andersen. Development and evaluation of a CERES-type model for winter oilseed rape. Field Crops Res. 57 : 95-111, 1998.

B. Gabrielle, P. Denoroy, G. Gosse, E. Justes, and M. N. Andersen. A model of leaf area development and senescence for winter oilseed rape. Field Crops Res. 57 : 209-222, 1998.

B. Gabrielle and S. Bories. Theoretical appraisal of field-capacity based in ltration model and their scale parameters. Transport in Porous Media 35 : 129-147, 1999.

C. Hénault and J.C. Germon. NEMIS, a predictive model of denitrification on the field scale. European Journal of Soil Science 51 (2) : 257-270 2000

B. Leviel, C. Crivineanu, and B. Gabrielle. CERES-Beet, a prediction model for sugar beet yield and environnemental impact. Adv. Sugar Beet Res., 5 : 143-152, 2003.

J. Cortinovis, Etude expérimentale et modélisation des émissions biogéniques d'oxydes d'azote et d'isoprène depuis les écosystèmes naturels et aménagés: impact sur l'ozone. Ph. D. Thesis, Université Paul Sabatier, Toulouse, 2004.

C. Hénault, F. Bizouard, P. Laville, B. Gabrielle, B. Nicoullaud, J. C. Germon, and P. Cellier. Predicting in situ soil N2O emissions using NOE algorithm and soil data base. Global Change Biol. 11 : 115-127, 2005.

Laville, P.; Hénault, C.; Gabrielle, B. & Serça, D. Measurement and modelling of NO fluxes on maize and wheat crops during their growing seasons: effect of crop management Nutr. Cycl. Agroecoecos., 72: 159 - 171,2005.

B. Gabrielle; Laville, P.; Hénault, C.; Nicoullaud, B. & Germon, J. C. Simulation of nitrous oxide emissions from wheat-cropped soils using CERES Nutrient Cycling Agroecosys., 2006, 74, 133-146.

Lehuger, S.; Gabrielle, B.; Cellier, P.; Loubet, B.; Roche, R.; Béziat, P.; Ceschia, E. & Wattenbach, M. Modelling the net carbon exchanges of crop rotations in Europe submitted to Agric. Ecosys. Environ. (May 2009), 2009
Rolland, M.-N.; Gabrielle; Laville, P.; Cellier, P.; Serça, D. & Cortinovis, J. Modeling of nitric oxide emissions from temperate agricultural ecosystems. Nutrient Cycling in Agroecosystems, 2008, 80, 75-93.

## Model testing
Z. Popova, B. Leviel, T. Mitova, B. Gabrielle, and M. Kercheva. Calibration and validation of CERES model of wheat ecosystem located in the Sophia region. J. Balkan Ecology 3 : 53-61, 2000.

Gabrielle, B., R. Roche, P. Angas, C. Cantero-Martinez, L. Cosentino, M. Mantineo, M. Langensiepen, C. Hénault, P. Laville, B. Nicoullaud, and G. Gosse. A priori parameterisation of the CERES soil-crop models and tests against several european data sets. Agronomie 22 : 119-132, 2002.

Gabrielle, B., B. Mary, R. Roche, P. Smith, and G. Gosse. Simulation of carbon and nitrogen dynamics in arable soils : a comparison of approaches. Eur. J. Agron. 18 : 107-120, 2002.

Lehuger, S.; Gabrielle, B.; van Oijen, M.; Makowski, D.; Germon, J.-C.; Morvan, T. & Hénault, C. Bayesian calibration of the nitrous oxide emission module of an agro-ecosystem model. Agriculture, Ecosystems & Environment, 2009, 133, 208 - 222.
Lehuger, S.; Gabrielle, B.; Laville, P.; Lamboni, M.; Loubet, B. & Cellier, P. Predicting and mitigating the net greenhouse gas emissions of crop rotations in Western Europe Agricultural Forest Meteorology, 2011, in press.
Wattenbach, M.; Sus, O.; Vuichard, N.; Lehuger, S.; Gottschalk, P.; Li, L.; Leip, A.; Williams, M.; Tomelleri, E.; Kutsch, W. L.; Buchmann, N.; Eugster, W.; Dietiker, D.; Aubinet, M.; Ceschia, E.; Béziat, P.; Grünwald, T.; Hastings, A.; Osborne, B.; Ciais, P.; Cellier, P. & Smith, P. The carbon balance of European croplands: A cross-site comparison of simulation models. Agriculture, Ecosystems & Environment, 2010, 139, 419 - 453.

## Parameterization, sensitivity and uncertainty analysis
Drouet, J.-L.; Capian, N.; Fiorelli; Blanfort, V.; Capitain, M.; Duretz, S.; Gabrielle, B.; Martin, R.; Lardy, R.; Cellier, P. & Soussana, J.-F. Sensitivity analysis for models of greenhouse gas emissions at farm level. Case study of N2O emissions simulated by the CERES-EGC model Environ. Pollution, 2011, (in press)

Gabrielle et al (1995) on soil hydrodynamic properties.

Gabrielle, B. Sensitivity and uncertainty analysis of a static denitrification model. In Wallach, D.; Makowski, D. & Jones, J. (ed.) Working with dynamic crop models. Evaluation, analysis, parameterization, and applications, Elsevier, 2006, 359-366.
Gabrielle, B. Modélisation des cycles des éléments eau-carbone-azote dans un système sol-plante, et application à l'estimation des bilans environnementaux des grandes cultures. Ph D Thesis, Ecole Centrale Paris, 1996.
Lamboni, M.; Makowski, D.; Lehuger, S.; Gabrielle, B. & Monod, H. Multivariate global sensitivity analysis for dynamic crop models. Field Crops Research, 2009, 113, 312 - 320.
Rolland et al (2010) for soil type, climate and fertilizer N rates in relation to nitric oxide emissions.
Model application
Z. Popova, M. Kercheva, B. Leviel, and B. Gabrielle. CERES model application to assess nitrogen leaching in wheat ecosystem. J. Balkan Ecology 3 : 62-67, 2000.

B. Gabrielle, J. Da-Silveira, S. Houot, and C. Francou. Simulating urban waste compost impact on C-N dynamics using a biochemical index. J. Envion. Qual. 33 :2333-2342, 2004.

B. Gabrielle, J. Da-Silveira, S. Houot, and J. Michelin. Field-scale modelling of C-N dynamics in soils amended with municipal waste composts. Agric. Ecosys. Environ, 2005, 110, 289-299.

B. Gabrielle; Laville, P.; Duval, O.; Nicoullaud, B.; Germon, J. C. & Hénault, C. Process-based modeling of nitrous oxide emissions from wheat-cropped soils at the sub-regional scale Global Biogeochemical Cycles, 2006, 20, GB4018.

Gabrielle, B. & Gagnaire, N. Life-cycle assessment of straw use in bio-ethanol production: a case-study based on deterministic modelling Biomass and Bioenergy, 2008, 32, 431-441.

Rolland, M.-N.; Gabrielle, B.; Laville, P.; Cellier, P.; Beekmann, M.; Gilliot, J.-M.; Michelin, J.; Hadjar, D. & Curci, G. Biophysical modelling of NO emissions from agricultural soils in northern France for use in regional chemistry-transport modelling. Environ. Pollution, 2010, 158, 711-722.

Durandeau, S.; Gabrielle, B.; Godard, C.; Jayet, P. A. & Lebas, C. Coupling biophysical and micro-economic models to assess nitrous oxide emissions from cropland and the effects of mitigation measures on greenhouse emissions. Climatic Change (in press), 2009
Miscellaneous
Reinhardt G.A., 2000, Bioenergy for Europe. Which ones fit best?, Rapport final Projet Biofit, contrat CT98-3832, IFEU, Heidelberg (PDF version).

S. Pérez. Bilan azoté d'une culture de sorgho sucrier à l'échelle de la parcelle. Master's thesis, Mémoire de fin d'études d'ingénieur, Ecole Polytechnique Fédérale de Lausanne, 2001 ( zipped MS Word file 2.3 Mb).

Jullien, A.; Roche, R.; Jeuffroy, M.; bf B. Gabrielle & Huet, P. Guérif, M. & King, D. (ed.) Intérêts de l'utilisation conjointe des modèles CERES et AZODYN pour raisonner la modulation de la fertilisation azotée Agriculture de précision, Editions Quae, collection Science Update, 2007, 249-266
B. Gabrielle, N. Gagnaire. Simulation de l’effet de la valorisation des pailles sur l’Analyse de Cycle de Vie du bioéthanol de blé . Rapport de fin de contrat, AGRICE, 2005 (PDF  440 Kb).

S. Lehuger. Evaluation environnementale de la substitution, en élevage bovin laitier, du tourteau de soja importé par du tourteau de colza produit localement. Mémoire de fin d'études d'ingénieur, Ecole Supérieure d'Agriculture, Angers, 2005. (PDF intégral - Condensé).

Hardelin, J. Modélisation intégrée économie et environnement : évaluation de différentes mesures de réduction de la pollution azotée en grandes cultures. DEA Économie de l'environnement et des ressources naturelles, ENGREF - EHESS - INA P-G - ENPC - Paris X - Ecole Polytechnique, 2005 (PDF).
