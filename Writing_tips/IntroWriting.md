A few tips and tricks about scientific writing, a daunting endeavor especially the first time around... But don't worry you will get there all right !
## Getting started
The way scientific papers are structured may lead you to think you should start by drafting the introduction, then segue with Material and Methods, go through Results (and or Discussion?) and wrap up with your conclusion.

## Picking a journal
A tricky but defining moment... Worth a good discussion with your supervisor.s if you are a PhD candidate, or you co-authors in general. Given the current push, or even imperative for open science, at institutional or regulatory level, you should strive to ensure your paper will be accessible as soon as it is published - whether from the publishers' web site or the repository where you will upload it. 
The thing is, there are different types of open-access (from diamond to gold), and each journal has their own policy - see [here][https://www.youtube.com/watch?v=8fGRN5fa-Ks] for more info. Do not let yourself be lured by big names and impact factors... and avoid the dreaded article publication charges that come with it at all cost!

There also exist alternatives to the classical peer-review process, in the hands of journal editors and publishers (mostly commercial) which run the journals.
Peer Community In is a non-profit organisation of researchers (see [here][https://peercommunityin.org/]) offers free peer review, recommendation and publication of scientific articles. 

## Resources
