# External resources on LCA: open LCA, a specialized, open-access software package

## Getting started: installing the Open LCA software + database

OpenLCA is an open-access LCA package which can be used for any production system.

To install OpenLCA package: visit the [openlca.org web site](http://www.openlca.org), and click on the Download button. You will need to register. Follow the instructions in the [‘Getting started’ documentation file](http://www.openlca.org/wp-content/uploads/2017/04/1.6-Getting-started.pdf) which will be helpful.

You will then need to download several databases of background processes and impact assessment methods: visit the nexus.openlca.org web page and click on the "Databases" and then "Free databases" tabs on top of this page. You can choose the database (DB) most suitable for the value-chain you would like to assess. In general I would recommend installing the Environmental Footprint (PEF) DB, which is the most comprehensive one, and the openLCA LICA impact assessment methods (which you need for the impact characterization stage).

To install these databases under OpenLCA: start openLCA, right-click in Navigation menu and select ‘New database’. Give it the name you for instance and click on Finish. This will create a new database in the left panel of OpenLCA.

Now right-click on this new database, select ‘Import’ and then the ‘Linked Data (JSON-LD)’ option in the first window before clicking on the ‘Next’ button; point to the zip file you have just downloaded from NexusOpenLCA in the next box. The DB may take a while to install (anything between 30 and 60 minutes, so you have time to grab a coffee or read some LCA studies!) When it is done you can browse the products modelled in the database and start your analyses.

## Installing Ecoinvent and AgriBalyse

As a student or staff registered with Paris-Saclay you may access the proprietary LCI database ecoinvent, the most widely used DB in Europe, v3.9.1. licensed to AgroParisTech (for academic purposes). You may download the following database in JSON format, which 
combines ecoinvent and AgriBalyse (v3.1) - please [contact me](mailto:Benoit.Gabrielle@agroparistech.fr) if you need this database. To install it, first create a new database (name it ei_Agribalyse, for instance), and then use the import procedure described in the previous paragraph. Please note that the license applies to academic use only, and commercial uses are not possible.

## Basics of LCA modelling with OpenLCA

To get familiar with the way LCA is organized in OpenLCA, we suggest you take a look at the tutorial below
 (duration: 12 minutes). You should be able to navigate between flows, processes and product systems in your local copy of the LCA database after that.
 
[OpenLCA tutorial](https://youtu.be/kEosW6PceVg)

You may first take a look at the life-cycle impacts of a product in your data base (for instance bioethanol in the PEF database, or wheat grains in the AgriBalyse or Ecoinvent DB). Remember you can only analyze processes once they have been transformed into ‘product systems’, which feature a ‘Calculate’ button (for LCA calculations). Once you’ve clicked on it you will need to specify a type of Impact characterization method (from the LCIA set).

You may start with ‘CML Baseline’, which is a popular and "historical" method. You may compare the outcomes more recent methods such as ‘ILCD 2011 Mid-point’ or 'IMPACT 2002+', which also feature ‘End-point’ variant (closer to the final damages incurred by the product system assessed).

Tip: to export LCA results from the ‘Impact analysis’ sheet: copy table from Impact analysis sheet and paste it into your favorite spreadsheet software.

LCA results may also be normalized with respect to the average emissions at country or global levels (you should select a Normalization set in the Calculation window – you may use EU25 for instance). Try normalizing your results for ethanol and check out the environmental profile of this profile against fossil-based gasoline, for instance.
