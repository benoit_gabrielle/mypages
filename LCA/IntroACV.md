# Introduction à l'analyse en cycle de vie (ACV)

## Apprendre à penser 'cycle de vie'
La video ci-dessous introduit la notion de cycle de vie et le mode de pensée sous-jacent, à des fins d'évaluation environnementale et de durabilité des activités humaines.

[Comprendre la pensée cycle de vie](https://youtu.be/iHq0LioDFwg?list=PLYv2QGyt7v5BSOx9NbDTtSSKt9fXwMM_A)

Les videos qui suivent détaillent un exemple d'application à la gestion de l'eau, et illustrent les enjeux de l'évaluation cycle de vie pour les entreprises ou les pouvoirs publics.

[ACV et aide à la décision publique](https://youtu.be/XI-HRBpOic0?list=PLYv2QGyt7v5BSOx9NbDTtSSKt9fXwMM_A)

[ACV et aide à la décision pour l'entreprise](https://youtu.be/n-2t10W-tcw?list=PLYv2QGyt7v5BSOx9NbDTtSSKt9fXwMM_A)

Après avoir visualisé les vidéos vous pourrez consulter [une fiche de l'ADEME](noteACVexterne.pdf) résumant les caractéristiques de cette méthodologie.

Pour des éléments plus détaillés vous pourrez consulter [une page proposant un certain nombre de ressources](OpenLCA.md), centrée sur le logiciel libre OpenLCA, spécialisé en ACV (ici). Le logiciel est disponible en salle informatique.

Vous pourrez aussi examiner la communication sur la Responsabilité Sociétale des Entreprises (RSE) d'une entreprise comme [Veolia, par exemple](https://www.veolia.com/fr/groupe/responsabilite-societale-lentreprise).

