# Climate change and agriculture - design fiction exercise

## Online resources for your case studies :

* Canada

[Overview of agriculture (on Wikipedia)](https://en.wikipedia.org/wiki/Agriculture_in_Canada)

[Nationally-determined contribution to reduce GHG emissions.](https://www.canada.ca/en/environment-climate-change/news/2021/04/canadas-enhanced-nationally-determined-contribution.html)

* [Colombia](https://en.wikipedia.org/wiki/Agriculture_in_Colombia)

[Overview of agriculture (on Wikipedia)](https://en.wikipedia.org/wiki/Agriculture_in_Colombia)

[Nationally-determined contribution to reduce GHG emissions](https://www.minambiente.gov.co/cambio-climatico-y-gestion-del-riesgo/documentos-oficiales-contribuciones-nacionalmente-determinadas/) (in Spanish... You may check the Climate Tracker page on Colombia as an alternative).

* Côte d'Ivoire

[Overview of agriculture (on Wikipedia)](https://en.wikipedia.org/wiki/Agriculture_in_Ivory_Coast)

[Nationally-determined contribution to reduce GHG emissions](https://climatepromise.undp.org/what-we-do/where-we-work/cote-divoire) (with intro by UNDP).

* Germany

[Overview of agriculture (on Wikipedia)](https://en.wikipedia.org/wiki/Agriculture_in_Germany)

[Nationally-determined contribution to reduce GHG emissions (at EU level in fact).](https://unfccc.int/sites/default/files/NDC/2022-06/EU_NDC_Submission_December%202020.pdf)

* Indonesia

[Overview of agriculture (on Wikipedia)](https://en.wikipedia.org/wiki/Agriculture_in_Indonesia)

[Nationally-determined contribution to reduce GHG emissions.](https://unfccc.int/sites/default/files/NDC/2022-09/ENDC%20Indonesia.pdf)

## A bit of background on (enhanced) NDCs

Nationally determined contributions (NDCs) are at the heart of the Paris Agreement and the achievement of its long-term goals. NDCs embody efforts by each country to reduce national emissions and adapt to the impacts of climate change. The [Paris Agreement](https://unfccc.int/node/617)  (Article 4, paragraph 2) requires each Party to prepare, communicate and maintain successive nationally determined contributions (NDCs) that it intends to achieve. Parties shall pursue domestic mitigation measures with the aim of achieving the objectives of such contributions.

Source: UNFCC - [click here for more details](https://unfccc.int/process-and-meetings/the-paris-agreement/nationally-determined-contributions-ndcs).

Since their first releases after COP21, NDCs were periodically updated and enhanced to bridge the "emission reduction gap". 

The [Climate Tracker website](https://climateactiontracker.org/) is also a good source for a (critical) overview of countries' climate policies and targets.




